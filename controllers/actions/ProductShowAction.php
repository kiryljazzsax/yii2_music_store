<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19.06.2019
 * Time: 12:58
 */

namespace app\controllers\actions;


use app\components\ProductComponent;
use app\models\Product;
use yii\base\Action;
use yii\base\Component;

class ProductShowAction extends Action
{
    public function run()
    {
        /** @var ProductComponent $component */
        $component = \Yii::$app->product;

        $id = \Yii::$app->request->get('id');

        $product = $component->showProduct($id);

        return $this->controller->render('show', ['product' => $product]);
    }
}