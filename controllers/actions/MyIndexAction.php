<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14.06.2019
 * Time: 22:53
 */

namespace app\controllers\actions;


use yii\base\Action;

class MyIndexAction extends Action
{
    public function run()
    {
        return $this->controller->render('index');
    }
}