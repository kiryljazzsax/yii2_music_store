<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.06.2019
 * Time: 17:54
 */

namespace app\controllers\actions;


use app\components\UsersAuthComponent;
use Codeception\Exception\ElementNotFound;
use yii\base\Action;
use yii\web\NotFoundHttpException;

class UsersInfoAction extends Action
{
    public function run()
    {
        /** @var UsersAuthComponent $component */
        $component = \Yii::$app->usersAuth;

        $id = \Yii::$app->request->get('id');

        if (!$userInfo = $component->getUserInfo($id)) {
            throw new NotFoundHttpException('Пользователь не найден :(');
        }

        return $this->controller->render('info', ['userInfo' => $userInfo]);
    }
}