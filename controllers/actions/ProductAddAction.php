<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19.06.2019
 * Time: 12:42
 */

namespace app\controllers\actions;

use app\components\ProductComponent;
use app\models\Product;
use yii\base\Action;

class ProductAddAction extends Action
{
    public function run()
    {
        /** @var ProductComponent $component */
        $component = \Yii::createObject([
            'class' => ProductComponent::class,
            'productModel' => Product::class,
        ]);

        if (\Yii::$app->request->isPost) {

            /** @var Product $model */
            $model = $component->getModel(\Yii::$app->request->post());
            $component->addProduct($model);
        } else {

            $model = $component->getModel();
        }

        return $this->controller->render('add', ['product' => $model]);
    }
}