<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.06.2019
 * Time: 22:48
 */

namespace app\controllers\actions;


use app\components\UsersAuthComponent;
use yii\base\Action;

class UsersIndexAction extends Action
{
    public function run()
    {
        /** @var UsersAuthComponent $component */
        $component = \Yii::$app->usersAuth;

        $users = $component->showUsers();

        return $this->controller->render('index', ['users' => $users]);
    }
}