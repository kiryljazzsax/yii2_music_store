<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19.06.2019
 * Time: 20:11
 */

namespace app\controllers\actions;


use app\components\UsersAuthComponent;
use app\models\Users;
use yii\base\Action;
use yii\bootstrap\ActiveForm;
use yii\web\Response;

class UsersRegisterAction extends Action
{
    public function run()
    {
        /** @var UsersAuthComponent $component */
        $component = \Yii::createObject([
            'class' => UsersAuthComponent::class,
            'usersModel' => Users::class,
        ]);

        if (\Yii::$app->request->isPost) {
            $model = $component->getModel(\Yii::$app->request->post());
            $model->setScenario($model::SCENARIO_REGISTER);
            if (\Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            if ($component->registerUser($model)) {
                \Yii::$app->session->addFlash('success', 'Поздравляю вам удалось зарегестрироваться!');
                return $this->controller->redirect('/');
            } else {
                \Yii::$app->session->addFlash('alert', 'Вам не удалось зарегистрироваться :(');
            }

        } else {
            $model = $component->getModel();
        }

        return $this->controller->render('register', ['model' => $model]);
    }
}