<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18.06.2019
 * Time: 12:42
 */

namespace app\controllers\actions;


use app\components\ProductComponent;
use app\models\Product;
use yii\base\Action;

class ProductIndexAction extends Action
{
    public function run()
    {
        /** @var ProductComponent $component */
        $component = \Yii::$app->product;

        $products = $component->showProducts();

        return $this->controller->render('index', ['products' => $products]);
    }
}