<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14.06.2019
 * Time: 22:56
 */

namespace app\controllers\actions;


use app\components\TestComponent;
use yii\base\Action;
use app\models\Test;

class MyTestAction extends Action
{
    public function run()
    {

        if (\Yii::$app->request->isPost) {
            $component = \Yii::createObject([
                'class' => TestComponent::class,
                'testClass' => Test::class,
            ]);
            $model = \Yii::$app->test->getModel(\Yii::$app->request->post());
            $component->myTest($model);
        } else {
            $model = \Yii::$app->test->getModel();
        }

        return $this->controller->render('test', ['model' => $model]);
    }
}