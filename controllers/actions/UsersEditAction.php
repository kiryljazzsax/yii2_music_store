<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 21.06.2019
 * Time: 20:51
 */

namespace app\controllers\actions;


use app\components\UsersAuthComponent;
use app\models\Users;
use yii\base\Action;
use yii\web\Response;
use yii\bootstrap\ActiveForm;

class UsersEditAction extends Action
{
    public function run()
    {
        /** @var UsersAuthComponent $component */
        $component = \Yii::$app->usersAuth;

        $idUser = \Yii::$app->request->get('id');

        /** @var Users $model */
        $model = $component->getModel()::findIdentity($idUser);

        if (\Yii::$app->request->isPost) {
            $model->load(\Yii::$app->request->post());
            if ($component->updateUser($model)) {
                \Yii::$app->session->addFlash('success', 'Вы изменили свои данные');
                return $this->controller->redirect('/users/' . $model->id);
            } else {
                \Yii::$app->session->addFlash('alert', 'Попробуйте ещё раз! У вас не получилось поменять информацию!');
            }
        }

        return $this->controller->render('edit', ['model' => $model]);
    }
}