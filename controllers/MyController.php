<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11.06.2019
 * Time: 2:28
 */

namespace app\controllers;


use app\base\BaseController;
use app\controllers\actions\MyIndexAction;
use app\controllers\actions\MyTestAction;

class MyController extends BaseController
{
    public function actions()
    {
        return [
            'index' => ['class' => MyIndexAction::class],
            'test' => ['class' => MyTestAction::class],
        ];
    }
}