<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.06.2019
 * Time: 17:36
 */

namespace app\controllers;


use app\base\BaseController;
use app\controllers\actions\UsersEditAction;
use app\controllers\actions\UsersIndexAction;
use app\controllers\actions\UsersInfoAction;
use app\controllers\actions\UsersRegisterAction;

class UsersController extends BaseController
{
    public function actions()
    {
        return [
            'index' => ['class' => UsersIndexAction::class],
            'info' => ['class' => UsersInfoAction::class],
            'register' => ['class' => UsersRegisterAction::class],
            'edit' => ['class' => UsersEditAction::class],
        ];
    }
}