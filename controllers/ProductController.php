<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18.06.2019
 * Time: 10:32
 */

namespace app\controllers;

use app\base\BaseController;
use app\components\ProductComponent;
use app\controllers\actions\ProductAddAction;
use app\controllers\actions\ProductIndexAction;
use app\controllers\actions\ProductShowAction;

class ProductController extends BaseController
{
    public function actions()
    {
        return [
            'index' => ['class' => ProductIndexAction::class,],
            'add' => ['class' => ProductAddAction::class],
            'show' => ['class' => ProductShowAction::class],
        ];
    }
}