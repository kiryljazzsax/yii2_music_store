<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string $name
 * @property string $short_description
 * @property string $description
 * @property int $price
 * @property string $image
 * @property int $id_category
 * @property int $id_brand
 * @property string $date_added
 * @property string $date_updated
 * @property string $ID_UUID
 * @property int $status
 */
class ProductBase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'short_description', 'description', 'price', 'image', 'id_category', 'id_brand', 'ID_UUID', 'status'], 'required'],
            [['short_description', 'description'], 'string'],
            [['price', 'id_category', 'id_brand', 'status'], 'integer'],
            [['date_added', 'date_updated'], 'safe'],
            [['name'], 'string', 'max' => 150],
            [['image', 'ID_UUID'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'short_description' => Yii::t('app', 'Short Description'),
            'description' => Yii::t('app', 'Description'),
            'price' => Yii::t('app', 'Price'),
            'image' => Yii::t('app', 'Image'),
            'id_category' => Yii::t('app', 'Id Category'),
            'id_brand' => Yii::t('app', 'Id Brand'),
            'date_added' => Yii::t('app', 'Date Added'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'ID_UUID' => Yii::t('app', 'Id Uuid'),
            'status' => Yii::t('app', 'Status'),
        ];
    }
}
