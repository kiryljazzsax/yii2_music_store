<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18.06.2019
 * Time: 12:27
 */

namespace app\models;


/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string $name
 * @property string $short_description
 * @property string $description
 * @property int $price
 * @property string $image
 * @property int $id_category
 * @property int $id_brand
 * @property string $date_added
 * @property string $date_updated
 * @property string $ID_UUID
 * @property int $status
 */
class Product extends ProductBase
{
    public function rules()
    {
        return array_merge([
            [['image'], 'file', 'extensions' => ['jpg', 'png']],
        ], parent::rules());
    }
}