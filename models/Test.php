<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11.06.2019
 * Time: 2:49
 */

namespace app\models;

use yii\base\Model;

class Test extends Model
{
    public $title;

    function rules()
    {
        return [
            ['title', 'string', 'min' => 3],
        ];
    }
}