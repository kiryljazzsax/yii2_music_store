<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16.06.2019
 * Time: 21:37
 */
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii_music_store',
    'username' => 'root',
    'password' => 'vagrant',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];