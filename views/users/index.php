<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.06.2019
 * Time: 22:47
 */
use app\models\Users;
use yii\helpers\Html;
?>

<div class="row">
    <div class="col-md-6">
        <?php /** @var Users $user */?>
        <?php foreach ($users as $user): ?>
        <ul>
            <li>
                <?= $user->login ?>
            </li>
            <li>
                <?= $user->name ?>
            </li>
            <li>
                <?= $user->surname ?>
            </li>
            <li>
                <?= $user->email ?>
            </li>
        </ul>
        <p><?= Html::a('посмотреть профиль?', '/users/' . $user->id); ?></p>
        <?php endforeach; ?>
    </div>
</div>
