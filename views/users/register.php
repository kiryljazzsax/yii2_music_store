<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19.06.2019
 * Time: 20:12
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="row">
    <div class="col-md-4">
        <?php /** @var ActiveForm $form */ ?>
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'login', ['enableAjaxValidation' => true]); ?>
        <?= $form->field($model, 'name'); ?>
        <?= $form->field($model, 'surname'); ?>
        <?= $form->field($model, 'email'); ?>
        <?= $form->field($model, 'gender')->inline(true)->radioList(['male' => 'Male', 'female' => 'Female']); ?>
        <?= $form->field($model, 'password_hash', ['enableAjaxValidation' => true])->passwordInput(); ?>
        <?= $form->field($model, 'password_repeat', ['enableAjaxValidation' => true])->passwordInput(); ?>
        <?= Html::submitButton('зарегимся', ['class' => 'btn btn-success']); ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
