<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 22.06.2019
 * Time: 19:43
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="row">
    <div class="col-md-4">
        <?php /** @var ActiveForm $form */ ?>
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'login'); ?>
        <?= $form->field($model, 'name'); ?>
        <?= $form->field($model, 'surname'); ?>
        <?= $form->field($model, 'email'); ?>

        <?= Html::submitButton('поменяем данные', ['class' => 'btn btn-success']); ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>


