<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.06.2019
 * Time: 17:57
 */
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-md-6">
        <ul>
            <li>
                <?= $userInfo->login ?>
            </li>
            <li>
                <?= $userInfo->name ?>
            </li>
            <li>
                <?= $userInfo->surname ?>
            </li>
            <li>
                <?= $userInfo->email ?>
            </li>
        </ul>
    </div>
</div>
