<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18.06.2019
 * Time: 22:41
 */

use yii\helpers\Html;
use \app\models\Product;


?>
    <div class="row">
        <div class="col-md-4">
            <?php /** @var Product $product */ ?>
            <?php foreach ($products as $product): ?>
                <h3>Товар <?= $product->name ?></h3>
                <ul>
                    <li>
                        <?= $product->short_description ?>
                    </li>
                    <li>
                        <?= $product->description ?>
                    </li>
                    <li>
                        <?= $product->price ?>
                    </li>
                </ul>

                <?= Html::a('Просмотреть товар', '/product/' . $product->id) ?>

            <?php endforeach; ?>
        </div>
    </div>


<?= Html::a('Добаить товар?', '/product/add') ?>