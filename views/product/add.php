<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 19.06.2019
 * Time: 12:44
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div class="row">
    <div class="col-md-4">
        <?php $form = ActiveForm::begin([
            'options' => [
                'enctype' => 'multipart/form-data'
            ]
        ]); ?>

        <?= $form->field($product, 'name'); ?>
        <?= $form->field($product, 'short_description'); ?>
        <?= $form->field($product, 'description'); ?>
        <?= $form->field($product, 'price')->input('number'); ?>
        <?= $form->field($product, 'id_category')->input('number'); ?>
        <?= $form->field($product, 'id_brand')->input('number'); ?>
        <?= $form->field($product, 'ID_UUID'); ?>
        <?= $form->field($product, 'status')->input('number'); ?>
        <?= $form->field($product, 'image')->fileInput(); ?>

        <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>

        <?php ActiveForm::end(); ?>
    </div>
</div>
