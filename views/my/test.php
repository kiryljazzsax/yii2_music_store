<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11.06.2019
 * Time: 2:45
 */
use yii\bootstrap\ActiveForm;
?>

<div class="row">
    <div class="col-md-12">
        <h2>Проверим</h2>
        <?php $form=ActiveForm::begin(); ?>
        <?= $form->field($model, 'title');?>
        <div class="form-group">
            <button type="submit" class="btn btn-default">Отправить</button>
        </div>
        <?php ActiveForm::end();?>
    </div>
</div>