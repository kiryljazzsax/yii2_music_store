<?php

use yii\db\Migration;

/**
 * Class m190616_185054_createTableUsers
 */
class m190616_185054_createTableUsers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'login' => $this->string(150)->notNull(),
            'password' => $this->string(300)->notNull(),
            'name' => $this->string(150)->notNull(),
            'surname' => $this->string(150)->notNull(),
            'gender' => $this->string(150)->notNull(),
            'date_added' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'date_updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190616_185054_createTableUsers cannot be reverted.\n";

        return false;
    }
    */
}
