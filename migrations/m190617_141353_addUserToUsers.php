<?php

use yii\db\Migration;

/**
 * Class m190617_141353_addUserToUsers
 */
class m190617_141353_addUserToUsers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('users', [
            'login' => 'login',
            'password' => \Yii::$app->security->generatePasswordHash('12345'),
            'name' => 'Kiryl',
            'surname' => 'Matsenka',
            'gender' => 'male',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('users');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190617_141353_addUserToUsers cannot be reverted.\n";

        return false;
    }
    */
}
