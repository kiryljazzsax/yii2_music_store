<?php

use yii\db\Migration;

/**
 * Class m190618_081611_addTablesProductCategoryBrand
 */
class m190618_081611_addTablesProductCategoryBrand extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'name' => $this->string(150)->notNull(),
            'short_description' => $this->text()->notNull(),
            'description' => $this->text()->notNull(),
            'price' => $this->integer()->notNull(),
            'image' => $this->string(300)->notNull(),
            'id_category' => $this->integer()->notNull(),
            'id_brand' => $this->integer()->notNull(),
            'date_added' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'date_updated' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'ID_UUID' => $this->string(300)->notNull(),
            'status' => $this->integer()->notNull(),
        ]);

        $this->createTable('categories', [
            'id' => $this->primaryKey(),
            'name' => $this->string(150)->notNull(),
            'image' => $this->string(300)->notNull(),
            'description' => $this->text()->notNull(),
            'sale' => $this->integer()->notNull(),
            'parent_category_id' => $this->integer()->notNull(),
        ]);

        $this->createTable('brands', [
            'id' => $this->primaryKey(),
            'name' => $this->string(300)->notNull(),
            'description' => $this->text()->notNull(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('products');
        $this->dropTable('categories');
        $this->dropTable('brands');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190618_081611_addTablesProductCategoryBrand cannot be reverted.\n";

        return false;
    }
    */
}
