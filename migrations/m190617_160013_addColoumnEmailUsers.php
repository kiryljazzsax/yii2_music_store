<?php

use yii\db\Migration;

/**
 * Class m190617_160013_addColoumnEmailUsers
 */
class m190617_160013_addColoumnEmailUsers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'email', $this->string(150)->notNull()
            ->defaultValue('matsak_92@mail.ru'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'email');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190617_160013_addColoumnEmailUsers cannot be reverted.\n";

        return false;
    }
    */
}
