<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18.06.2019
 * Time: 10:30
 */

namespace app\components;

use app\models\Product;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use yii\base\Component;
use yii\web\UploadedFile;

class ProductComponent extends Component
{
    /** @var Product $productModel */
    public $productModel;

    /**
     * @param array $params [optional]
     * @return Product model
     */
    public function getModel($params = null)
    {
        $model = new $this->productModel;

        if ($params && is_array($params)) {
            $model->load($params);
        }

        return $model;
    }

    /**
     * @param $model Product
     * @return bool if success
     */
    public function addProduct(&$model): bool
    {
        /** @var Product $model */
        $model->date_added = date('Y-m-d H:i:s');
        $model->date_updated = date('Y-m-d H:i:s');
        $model->image = UploadedFile::getInstance($model, 'image');

        self::saveImage($model);

        if (!$model->validate()) {
            \Yii::$app->session->addFlash('alert', 'Заполните правильно форму!');
            return false;
        }

        if (!$model->save()) {
            $model->addError('name', 'Не удалось добавить товар в таблицу!');
            return false;
        }

        \Yii::$app->session->addFlash('success', 'Добавили товар!');
        return true;
    }

    /**
     * @param $model
     * @return bool if success
     */
    public function saveImage(&$model)
    {
        $path = \Yii::getAlias('@app/web/images/');
        if ($model->image) {
            $name = mt_rand(0, 9999) . time() . '.' . $model->image->getExtension();
            if (!$model->image->saveAs($path . $name)) {
                $model->addError('image', 'Файл не удалось переместить');
                return false;
            }
            $model->image = $name;

            return true;
        } else {
            return true;
        }
    }

    /**
     * @param $idProduct Product with property id
     * @return array Product
     */
    public function showProduct($idProduct)
    {
        return $this->getModel()::find()->andWhere(['id' => $idProduct])->one();
    }

    /**
     * @return array
     */
    public function showProducts()
    {
        return $this->getModel()::find()->all();
    }
}