<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.06.2019
 * Time: 17:28
 */

namespace app\components;


use app\models\Users;
use phpDocumentor\Reflection\DocBlock\Tags\Example;
use yii\base\Component;

class UsersAuthComponent extends Component
{
    /**
     * @var string class Users model
     */
    public $usersModel;

    public function getModel($params = null)
    {
        /** @var Users $model */
        $model = new $this->usersModel;

        if ($params && is_array($params)) {
            $model->load($params);
        }

        return $model;
    }

    public function getUserInfo($id)
    {
        return $this->getModel()::find()->andWhere(['id' => $id])->one();
    }

    public function registerUser(&$model)
    {
        /** @var Users $model */
        $model->date_added = date('Y-m-d H:i:s');
        $model->date_updated = date('Y-m-d H:i:s');
        $model->password = \Yii::$app->security->generatePasswordHash($model->password_hash);


        if (!$model->save()) {
            \Yii::$app->session->addFlash('alert', 'Что-то пошло не так вам не удалось зарегистрироваться');
            return false;
        }

        return $model;
    }

    /**
     * @param $model Users
     * @return bool true is success
     */
    public function updateUser(&$model):bool
    {
        $model->date_updated = date('Y-m-d H:i:s');

        if (!$model->validate()) {
            print_r($model->errors);
            return false;
        }

        if (!$model->update()) {
            return false;
        }
        return true;
    }

    public function showUsers()
    {
        return $this->getModel()::find()->all();
    }
}