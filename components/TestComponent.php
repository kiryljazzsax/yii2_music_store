<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14.06.2019
 * Time: 23:14
 */

namespace app\components;

use yii\base\Component;

class TestComponent extends Component
{
    public $testClass;

    public function getModel($param = null){
        $model = new $this->testClass;

        if ($param && is_array($param)) {
            $model->load($param);
        }
            return $model;
    }

    public function myTest(&$model):bool {

        return $model->validate();
    }
}